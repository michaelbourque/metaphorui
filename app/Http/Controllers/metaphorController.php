<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;

class metaphorController extends Controller
{

    /**
     * [getExchange estabish an interative session with an Exchange Service with the parameters specified in .env]
     * @return [Object] The Exchange Web Services interactive session
     */
    public function getExchange()
    {
        require( app_path() . '/Includes.php');

        $ews = new \ExchangeWebServices($host, $username, $password, \ExchangeWebServices::VERSION_2010);

        return $ews;
    }

    /**
     * [readMailBox uses the EWS Session to collect information about emails in the specific folder]
     * @param  [string] $folder Name of the folder to be processed
     * @param  [int]    $scale  Scale (max # of days) to be used for highlightling
     * @return [object]         An list object containing the details about each message in the folder
     */
    public function readMailBox($folder, $scale)
    {
        $ews = Self::getExchange();
        $folderId = Self::setFolderId($folder);

        $request = new \EWSType_FindItemType();
        $request->ItemShape = new \EWSType_ItemResponseShapeType();
        $request->ItemShape->BaseShape = \EWSType_DefaultShapeNamesType::ALL_PROPERTIES;
        $request->Traversal = \EWSType_ItemQueryTraversalType::SHALLOW;
        $request->ParentFolderIds = new \EWSType_NonEmptyArrayOfBaseFolderIdsType();
        $request->ParentFolderIds->FolderId = new \EWSType_FolderIdType();
        $request->ParentFolderIds->FolderId->Id = $folderId['Id'];

        $response = $ews->FindItem($request);
        $messages = null;
        $oldestMsgDate = null;
       
        if ($response->ResponseMessages->FindItemResponseMessage->RootFolder->TotalItemsInView > 0) {
            foreach ($response->ResponseMessages->FindItemResponseMessage->RootFolder->Items as $message) {
                if (count($message) > 1) {
                    unset($messageDetails);
                    foreach ($message as $msg) {
                        unset($messageDetails);
                        $thisMessageDate = $msg->DateTimeReceived;
                        $messageDetails['Id'] = $msg->ItemId->Id;
                        $messageDetails['Received'] = $msg->DateTimeReceived;
                        $messageDetails['Subject'] = $msg->Subject;
                        $messageDetails['HasAttachments'] = $msg->HasAttachments;
                        $messageDetails['Sender'] = $msg->Sender->Mailbox->Name;
                        $messageDetails['IsRead'] = $msg->IsRead;
                        $messageDetails['Created'] = $msg->DateTimeReceived;
                        $messages[] = $messageDetails;
                        $oldestMsgDate = Self::olderDate($oldestMsgDate, $thisMessageDate);
                    }
                } else {
                    unset($messageDetails);
                    $thisMessageDate = $message->DateTimeReceived;
                    $messageDetails['Id'] = $message->ItemId->Id;
                    $messageDetails['Received'] = $message->DateTimeReceived;
                    $messageDetails['Subject'] = $message->Subject;
                    $messageDetails['HasAttachments'] = $message->HasAttachments;
                    $messageDetails['Sender'] = $message->Sender->Mailbox->Name;
                    $messageDetails['IsRead'] = $message->IsRead;
                    $messageDetails['Created'] = $message->DateTimeReceived;
                    $messages[] = $messageDetails;
                    $oldestMsgDate = Self::olderDate($oldestMsgDate, $thisMessageDate);
                }
            }
        }
        if (!($messages==null)) {
            $msgCount = count($messages);
            for ($i=0; $i < $msgCount; $i++) {
            }
        } else {
            $msgCount = 0;
        }
        $messages['Mailbox'] = $folder;
        $messages['Icon'] = $folderId['Icon'];
        $messages['OldestMessageDate'] = $oldestMsgDate;
        $messages['Count'] = $msgCount;
        $messages['Scale'] = $scale;

        return $messages;
    }

    /**
     * [setFolderId description]
     * @param  string $folder The name of the folder
     * @return [object]       A list oject containing the ID and ChangeKey for the specified folder;     
     */
    public function setFolderId($folder)
    {
        unset($folderId);

        if ($folder == 'Inspections') {
            $folderId['Id'] = 'AAEuAAAAAAAaRHOQqmYRzZvIAKoAL8RaAwAWoN8/EABrR5HDiyLUOcPVAAASo9khAAA=';
            $folderId['ChangeKey'] = 'AQAAABYAAADBf4QGHbR0RKkh2AFJg5uqAAAAFR7J';
            $folderId['Icon'] = 'search';
        }
        if ($folder == 'Claims') {
            $folderId['Id'] = 'AQEuAAADGkRzkKpmEc2byACqAC/EWgMAFqDfPxAAa0eRw4si1DnD1QAAAQRNdwAAAA==';
            $folderId['ChangeKey'] = 'AQAAABYAAAAWoN8/EABrR5HDiyLUOcPVAAATXwkL';
            $folderId['Icon'] = 'money';
        }
        if ($folder == 'Info') {
            $folderId['Id'] = 'AQEuAAADGkRzkKpmEc2byACqAC/EWgMAFqDfPxAAa0eRw4si1DnD1QAAAQRNegAAAA==';
            $folderId['ChangeKey'] = 'AQAAABYAAAAWoN8/EABrR5HDiyLUOcPVAAATXvZA';
            $folderId['Icon'] = 'info';
        }
        if ($folder == 'Quotes') {
            $folderId['Id'] = 'AQEuAAADGkRzkKpmEc2byACqAC/EWgMAFqDfPxAAa0eRw4si1DnD1QAAAQRNcQAAAA==';
            $folderId['ChangeKey'] = 'AQAAABYAAADBf4QGHbR0RKkh2AFJg5uqAAAAB05q';
            $folderId['Icon'] = 'quote-right';
        }
        if ($folder == 'Junk') {
            $folderId['Id'] = 'AQEuAAADGkRzkKpmEc2byACqAC/EWgMAFqDfPxAAa0eRw4si1DnD1QAAAQRNfQAAAA==';
            $folderId['ChangeKey'] = 'AQAAABYAAAAWoN8/EABrR5HDiyLUOcPVAAACwbxZ';
            $folderId['Icon'] = 'trash';
        }
        if ($folder == 'Drop File') {
            $folderId['Id'] = 'AAEuAAAAAAAaRHOQqmYRzZvIAKoAL8RaAwAWoN8/EABrR5HDiyLUOcPVAAAShk2vAAA=';
            $folderId['ChangeKey'] = 'AQAAABYAAAAWoN8/EABrR5HDiyLUOcPVAAATZhmW';
            $folderId['Icon'] = 'chevron-down';
        }
        if ($folder == 'Pull File') {
            $folderId['Id'] = 'AAEuAAAAAAAaRHOQqmYRzZvIAKoAL8RaAwAWoN8/EABrR5HDiyLUOcPVAAASiHObAAA=';
            $folderId['ChangeKey'] = 'AQAAABYAAAAWoN8/EABrR5HDiyLUOcPVAAATXvZI';
            $folderId['Icon'] = 'chevron-up';
        }
        if ($folder == 'Request') {
            $folderId['Id'] = 'AQEuAAADGkRzkKpmEc2byACqAC/EWgMAFqDfPxAAa0eRw4si1DnD1QAAAQRNgwAAAA==';
            $folderId['ChangeKey'] = 'AQAAABYAAAAWoN8/EABrR5HDiyLUOcPVAAADvOXU';
            $folderId['Icon'] = 'cog';
        }
        if ($folder == 'IT') {
            $folderId['Id'] = 'AAEuAAAAAAAaRHOQqmYRzZvIAKoAL8RaAwAWoN8/EABrR5HDiyLUOcPVAAACw5WvAAA=';
            $folderId['ChangeKey'] = 'AQAAABYAAADBf4QGHbR0RKkh2AFJg5uqAAAAGIjG';
            $folderId['Icon'] = 'laptop';
        }
        if ($folder == 'Professional') {
            $folderId['Id'] = 'AQEuAAADGkRzkKpmEc2byACqAC/EWgMAFqDfPxAAa0eRw4si1DnD1QAAAQRNgAAAAA==';
            $folderId['ChangeKey'] = 'AQAAABYAAADBf4QGHbR0RKkh2AFJg5uqAAAAGKDu';
            $folderId['Icon'] = 'briefcase';
        }
        return $folderId;
    }

    /**
     * [olderDate description]
     * @param  string $date1 one of the dates to be compared
     * @param  string $date2 one of the dates to be compared
     * @return date          The older of the two provided dates, properly formatted
     */
    public function olderDate($date1, $date2)
    {
        $newDate1 = new \DateTime($date1);
        $newDate2 = new \DateTime($date2);
        //dd($newDate1->format('Y-m-d'), ' ', $newDate2->format('Y-m-d'));
        if ($newDate1 < $newDate2) {
            return $newDate1->format('Y-m-d');
        }
        if ($newDate2 < $newDate1) {
            return $newDate2->format('Y-m-d');
        }
        if ($newDate1 == $newDate2) {
            return $newDate1->format('Y-m-d');
        }

    }

    public function updateBoxes()
    {
        $messages[] = Self::readMailBox('Inspections', 5);
        $messages[] = Self::readMailBox('Claims', 5);
        $messages[] = Self::readMailBox('Quotes', 5);
        $messages[] = Self::readMailBox('Info', 5);
        $messages[] = Self::readMailBox('Drop File', 3);
        $messages[] = Self::readMailBox('Pull File', 2);
        $messages[] = Self::readMailBox('Request', 1);
        $messages[] = Self::readMailBox('IT', 1);
        $messages[] = Self::readMailBox('Junk', 30);
        $messages[] = Self::readMailBox('Professional', 1);

        return $messages;
    }

    /**
     * [getCharts Gets the data releveant to an individual Public Folder ]
     * @return view return the view with the data object (list of message details for each folder)
     */
    public function getBoxes()
    {

        $messages[] = Self::readMailBox('Inspections', 5);
        $messages[] = Self::readMailBox('Claims', 5);
        $messages[] = Self::readMailBox('Quotes', 5);
        $messages[] = Self::readMailBox('Info', 5);
        $messages[] = Self::readMailBox('Drop File', 3);
        $messages[] = Self::readMailBox('Pull File', 2);
        $messages[] = Self::readMailBox('Request', 1);
        $messages[] = Self::readMailBox('IT', 1);
        $messages[] = Self::readMailBox('Junk', 30);
        $messages[] = Self::readMailBox('Professional', 1);

        return View::make('index')->with(array('messages' => $messages));
    }

    /**
     * [getMessageList get a list of messages and relevent details for a specified mail box (Public Folder)]
     * @param  Request $request formdata passed to the Function via ajax request. Expected: $folder as 'mailbox'
     * @return array           an array containing the list of messages and individual details
     */
    public function getMessageList(Request $request)
    {

        $folder = $request->input('mailbox');

        $folderId = Self::setFolderId($folder);


        $ews = Self::getExchange();

        $request = new \EWSType_FindItemType();

        $request->ItemShape = new \EWSType_ItemResponseShapeType();
        $request->ItemShape->BaseShape = \EWSType_DefaultShapeNamesType::ALL_PROPERTIES;

        $request->Traversal = \EWSType_ItemQueryTraversalType::SHALLOW;

        $request->ParentFolderIds = new \EWSType_NonEmptyArrayOfBaseFolderIdsType();
        $request->ParentFolderIds->FolderId = new \EWSType_FolderIdType();
        $request->ParentFolderIds->FolderId->Id = $folderId['Id'];


        // sort order
        $request->SortOrder = new \EWSType_NonEmptyArrayOfFieldOrdersType();
        $request->SortOrder->FieldOrder = array();



        $order = new \EWSType_FieldOrderType();
        // sorts mails so that oldest appear first
        // more field uri definitions can be found from types.xsd (look for UnindexedFieldURIType)
        $order->FieldURI = new \EWSType_FieldURIOrConstantType();
        $order->FieldURI->FieldURI = 'item:DateTimeReceived';
        $order->Order = 'Ascending';

        $request->SortOrder->FieldOrder[] = $order;

        $response = $ews->FindItem($request);

        //echo '<pre>'.print_r($response->ResponseMessages->FindItemResponseMessage->RootFolder, true).'</pre>';

        if (isset($response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message)) {
            if (count($response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message) > 1) {
                foreach ($response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message as $message) {
                    $sendback[] = (
                        array(
                            'Id'    => $message->ItemId->Id,
                            'Subject' => $message->Subject,
                            'Sender' => $message->Sender,
                            'Importance' => $message->Importance,
                            'DateTimeReceived' => $message->DateTimeReceived,
                            'Size' => $message->Size,
                            'Attachments' => $message->HasAttachments,
                        )
                    );
                }
            } elseif (count($response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message) == 1) {
                $sendback[] = (
                    array(
                        'Id'    => $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message->ItemId->Id,
                        'Subject' => $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message->Subject,
                        'Sender' => $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message->Sender,
                        'Importance' => $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message->Importance,
                        'DateTimeReceived' => $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message->DateTimeReceived,
                        'Size' => $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message->Size,
                        'Attachments' => $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Message->HasAttachments,
                    )
                );
            }
        } else {
                $sendback[] = 'Empty';
        }


        return $sendback;
    }


    public function getMessage(Request $request)
    {

        $ews = Self::getExchange();
        $requestClone = $request;
        $message_id = $request->input('messageId');
        $attachmentInfo = null;
        
        //echo '<pre>'.print_r($attachmentInfo, true).'</pre>';

        // Build the request for the parts.
        $request = new \EWSType_GetItemType();
        $request->ItemShape = new \EWSType_ItemResponseShapeType();
        $request->ItemShape->BaseShape = \EWSType_DefaultShapeNamesType::ALL_PROPERTIES;
        // You can get the body as HTML, text or "best".
        $request->ItemShape->BodyType = \EWSType_BodyTypeResponseType::HTML;

        // Add the body property.
        $body_property = new \EWSType_PathToUnindexedFieldType();
        $body_property->FieldURI = 'item:Body';
        $request->ItemShape->AdditionalProperties = new \EWSType_NonEmptyArrayOfPathsToElementType();
        $request->ItemShape->AdditionalProperties->FieldURI = array($body_property);

        $request->ItemIds = new \EWSType_NonEmptyArrayOfBaseItemIdsType();
        $request->ItemIds->ItemId = array();

        // Add the message to the request.
        $message_item = new \EWSType_ItemIdType();
        $message_item->Id = $message_id;
        $request->ItemIds->ItemId[] = $message_item;


        $response = $ews->GetItem($request);

        if ($response->ResponseMessages->GetItemResponseMessage->ResponseCode == 'NoError' &&
            $response->ResponseMessages->GetItemResponseMessage->ResponseClass == 'Success') {

            $message = $response->ResponseMessages->GetItemResponseMessage->Items->Message;

            $attachmentInfo = Self::getAttachmentInfo($requestClone);
        }

        //echo '<pre>'.print_r($response, true).'</pre>';

        if ($response->ResponseMessages->GetItemResponseMessage->ResponseCode == 'NoError' && $response->ResponseMessages->GetItemResponseMessage->ResponseClass=="Success") {

            if (isset($response->ResponseMessages->GetItemResponseMessage->Items->Message->ToRecipients->Mailbox->Name)) {
                $recipients[] = [
                                    $response->ResponseMessages->GetItemResponseMessage->Items->Message->ToRecipients->Mailbox->Name,
                                    $response->ResponseMessages->GetItemResponseMessage->Items->Message->ToRecipients->Mailbox->EmailAddress
                                 ];
            } elseif (isset($response->ResponseMessages->GetItemResponseMessage->Items->Message->ToRecipients->Mailbox)) {
                $mailboxes = $response->ResponseMessages->GetItemResponseMessage->Items->Message->ToRecipients->Mailbox;
                foreach ($mailboxes as $mailbox) {
                    $recipients[] = [$mailbox->Name, $mailbox->EmailAddress];
                }
            } else {
                $recipients[] = [
                    'BCC',
                    'No Recipient'
                ];
            }
            $sendback[] = (
                array(
                    'Body' => $response->ResponseMessages->GetItemResponseMessage->Items->Message->Body,
                    'Sender' => $response->ResponseMessages->GetItemResponseMessage->Items->Message->Sender->Mailbox->EmailAddress,
                    'LastModifiedName' => $response->ResponseMessages->GetItemResponseMessage->Items->Message->LastModifiedName,
                    'LastModifiedTime' => $response->ResponseMessages->GetItemResponseMessage->Items->Message->LastModifiedTime,
                    'AttachmentInfo' => $attachmentInfo,
                    'ChangeKey' => $response->ResponseMessages->GetItemResponseMessage->Items->Message->ItemId->ChangeKey,
                    'ResponseMessage' => $response->ResponseMessages->GetItemResponseMessage->ResponseClass,
                    'Recipients' => $recipients,
                )
            );

            return $sendback;

        } else {
            return $response->ResponseMessages->GetItemResponseMessage->MessageText;
        }
    }

    public function getAttachmentInfo(Request $request)
    {
        $ews = Self::getExchange();

        $messageId = $request->input('messageId');
        
        $request = new \EWSType_GetItemType();

        $request->ItemShape = new \EWSType_ItemResponseShapeType();
        $request->ItemShape->BaseShape = \EWSType_DefaultShapeNamesType::ALL_PROPERTIES;
        $request->ItemShape->IncludeMimeContent = true;

        $request->ItemIds = new \EWSType_NonEmptyArrayOfBaseItemIdsType();
        $request->ItemIds->ItemId = new \EWSType_ItemIdType();
        $request->ItemIds->ItemId->Id = $messageId;

        //dd($request);

        $response = $ews->GetItem($request);

        //echo '<pre>'.print_r($response, true).'</pre>';
        
        $result = null;

        if ($response->ResponseMessages->GetItemResponseMessage->ResponseCode == 'NoError' && $response->ResponseMessages->GetItemResponseMessage->ResponseClass == 'Success') {

            $message = $response->ResponseMessages->GetItemResponseMessage->Items->Message;

            //dd($message->Attachments->ItemAttachment);

            if (!empty($message->Attachments->FileAttachment)) {
                //dd($message->Attachments->FileAttachment);
                
                // FileAttachment attribute can either be an array or instance of stdClass...
                $attachments = array();

                if (is_array($message->Attachments->FileAttachment) === false) {
                    $attachments[] = $message->Attachments->FileAttachment;
                } else {
                    $attachments = $message->Attachments->FileAttachment;
                }

                foreach ($attachments as $attachment) {
                    $result[] = array(
                        'Name' => $attachment->Name,
                        'Size' => $attachment->Size,
                        'Id' => $attachment->AttachmentId->Id,
                        //'Type' => $attachment->ContentType,
                        'Inline' => $attachment->IsInline,
                    );
                }
            }
            if (!empty($message->Attachments->ItemAttachment)) {
                //dd($message->Attachments->FileAttachment);
                
                // FileAttachment attribute can either be an array or instance of stdClass...
                $attachments = array();

                if (is_array($message->Attachments->ItemAttachment) === false) {
                    $attachments[] = $message->Attachments->ItemAttachment;
                } else {
                    $attachments = $message->Attachments->ItemAttachment;
                }

                foreach ($attachments as $attachment) {
                    $result[] = array(
                        'Name' => $attachment->Name,
                        'Size' => $attachment->Size,
                        'Id' => $attachment->AttachmentId->Id,
                        //'Type' => $attachment->ContentType,
                        'Inline' => $attachment->IsInline,
                    );
                }
            }
            return $result;
        }
    }

    public function getAttachment(Request $request)
    {
        $ews = Self::getExchange();
        $save_dir = env('ATTACHMENT_FOLDER', '__DIR__');
        $attachmentId = $request->input('attachmentId');

        $request = new \EWSType_GetAttachmentType();
        $request->AttachmentIds = new \EWSType_NonEmptyArrayOfRequestAttachmentIdsType();
        $request->AttachmentIds->AttachmentId = new \EWSType_RequestAttachmentIdType();
        $request->AttachmentIds->AttachmentId->Id = $attachmentId;

        $response = $ews->GetAttachment($request);
        // Assuming response was successful ...
        $attachments = $response->ResponseMessages->GetAttachmentResponseMessage->Attachments;


        if (isset($attachments->FileAttachment)) {
            $content = $attachments->FileAttachment->Content;
            $filename = preg_replace("/[^a-z0-9\.]/", "", strtolower($attachments->FileAttachment->Name));
            file_put_contents($save_dir.'/'. $filename, $content);

            $fileUrl = url('/') . '/attachments/' . $filename;
        } else {
            if ($attachments->ItemAttachment->Message) {
                if ($attachments->ItemAttachment->Message->ItemClass=="IPM.Note") {
                    $meta['meta'] = true;
                    $meta['sender'] = $attachments->ItemAttachment->Message->Sender;
                    $meta['subject'] = $attachments->ItemAttachment->Message->Subject;
                    $meta['hasattachments'] = $attachments->ItemAttachment->Message->HasAttachments;
                    $meta['attachments'] = $attachments->ItemAttachment->Message->Attachments;
                    $meta['body'] = $attachments->ItemAttachment->Message->Body;
                    //var_dump($meta);
                    //dd($attachments->ItemAttachment->Message->Attachments);
                    return json_encode($meta);
                }
            };
        }
        return json_encode($fileUrl);
    }
}
