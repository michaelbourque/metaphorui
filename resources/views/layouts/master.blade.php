<!DOCTYPE html>
<html>
<head>
  <title>
    @section('title')
    AMF MEtaPhOR
    @show
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link href="{{ URL::asset('assets/bscustom/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ URL::asset('assets/bscustom/css/bootstrap-theme.min.css') }}" rel="stylesheet"/>
  <link href="{{ URL::asset('assets/css/app-custom.css') }}" rel="stylesheet"/>  
</head>
<body>
<nav class="navbar navbar-darkgrey navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">MEtaPhOR</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
               
            </ul>
        </div>
    </div>
</nav>

  <!-- Alert -->
  <div class="container-fluid" id='alert_container'>
    <div class="row">
      <div class="col-xs-12">
        <div id="alert">
          @if(Session::get('message'))
          <div class="alert alert-{{ Session::get('type') }} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <span class="{{ Session::get('class') }}"></span> {{ Session::get('message') }}
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>

  <script src="{{ URL::asset('assets/js/jquery-2.1.4.min.js') }}"></script>
  <script src="{{ URL::asset('assets/bscustom/js/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('assets/js/app-custom.js') }}"></script>  
  <script src="{{ URL::asset('assets/js/app-custom-functions.js') }}"></script>  
  <script src="{{ URL::asset('assets/js/app-custom-ajax.js') }}"></script>   
  <script src="{{ URL::asset('assets/js/app-custom-showmsg.js') }}"></script> 

  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12">
        @yield('content')
      </div>
    </div>
  </div>
    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place footer content here.</p>
      </div>
    </footer>  
</body>
</html>