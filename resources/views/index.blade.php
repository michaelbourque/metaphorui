@extends("layouts.master")

@section("title")
@parent
- Home
@stop

@section("content")
<h2>
    <span class="pull-right">
        <div class="btn-group">
            <button type="button" class="btn btn-darkgrey">Save</button>
            <button type="button" class="btn btn-darkgrey">Home</button>
        </div>
    </span>
    <i class="fa fa-exchange fa-2x pull-left"></i>Microsoft Exchange Public fOlder pRoject <br/><small><em>A.M. Fredericks Underwriting Management Ltd.</em></small>
</h2>
<hr/>
@if(Session::get('message'))
    <div class="alert alert-{{ Session::get('type') }} alert-dismissible" data-fade="{{ Session::get('fade') ? "false":"true" }}" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <span class="{{ Session::get('class') }}"></span> {{ Session::get('message') }}
    </div>
@endif
<!--
<span class="visible-xs">Extra Small</span><span class="visible-sm">Small</span><span class="visible-md">Medium</span><span class="visible-lg">Large</span>
-->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
        <div class="panel panel-darkgrey">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="pull-right">
                        Folders: {{ count($messages) }}
                    </span>
                    Public Folders
                </h4>
            </div>
            <div class="panel-body">
                <div class="list-group list-group-custom">
                @foreach ($messages as $message)                
                    <a href="#" class="list-group-item mailbox-list-item" data-mailbox="{{$message['Mailbox']}}" data-scale="{{$message['Scale']}}"><i class="fa fa-{{$message['Icon']}}"></i><span class="mailboxname">{{$message['Mailbox']}}</span><span class="oldestmessage pull-right">{{$message['OldestMessageDate']}}</span><span class="messagecount pull-right">{{$message['Count']}}</span></a>
                @endforeach
                </div>
            </div>
        </div>

 <!--       <div class="panel panel-darkgrey">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="pull-right">
                        Data Value: 0
                    </span>
                    Panel Heading
                </h4>
            </div>
            <div class="panel-body">
                <div class="list-group list-group-custom">
                    <a href="#" class="list-group-item"><i class="fa fa-users"></i>Element 1</a>
                    <a href="#" class="list-group-item"><i class="fa fa-university"></i>Element 2 </a>
                </div>
            </div>
        </div>    -->


    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
        <div class="panel panel-darkgrey" id="message-details-panel">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="mailbox-count-panel pull-right">
                        Data Value: 0
                    </span>
                    <span class="mailbox-name-panel">Panel Heading</span>
                </h4>
            </div>
            <div class="panel-body">
                <div class="list-group list-group-custom list-group-message-details">
                    <!--<li class="list-group-item cs_dropdown pointer" data-open="false"><span class="text-grey"><i class="fa fa-paw"></i>Element 1 <span class="pull-right"><i class="fa fa-caret-right cs_dropdown_caret"></i></span></span></li>
                    <span class="cs_dropdown_body" style="display:none;">
                        <div class="list-group list-group-custom">
                                                        <a href="#" class="list-group-item sub-item"><i class="fa fa-tasks"></i>Sub-Element 1</a>
                                                        <a href="#" class="list-group-item sub-item"><i class="fa fa-lock"></i>Sub-Element 2</a>
                                                        <a href="#" class="list-group-item sub-item"><i class="fa fa-warning"></i>Sub-Element 3</a>
                                                        <a href="#" class="list-group-item sub-item"><i class="fa fa-share-alt"></i>Sub-Element 4</a>
                                                        <a href="#" class="list-group-item sub-item"><i class="fa fa-send"></i>Sub-Element 5</a>
                                                    </div>
                    </span>-->
                    <div class="list-group-item">
                        <span class="messageid">#</span>
                        <span class="messagestatus">!</span>
                        <span class="messagesender">Sender</span>
                        <span class="messagesubject">Subject</span>
                        <span class="messageage">Age</span>
                        <span class="messagesent hidden-md">Sent</span>
                        <span class="messagesize hidden-md">Size</span>
                        <span class="messageattachment"><i class="fa fa-paperclip"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="msgModal" class="modal fade" role="dialog">
</div>



@stop