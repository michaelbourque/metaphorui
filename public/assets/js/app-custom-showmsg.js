function showMsg ( $prevMsgId, $messageId, $nextMsgId, $msgCount, $msgIndex, $hasAttachments, $subject, $sender,
                   $datetime, $mailbox, status, data) {
    
    var $changeKey = (data[0].ChangeKey);
    var $recipientNames = '';

    if (status != 'error') {
        $.each(data, function(index, value) {

            var attachments = value.AttachmentInfo;

            $.each(value.Recipients, function(indexx, recipients) {
                $recipientNames += '<span class="recipientname"><strong>' + recipients[0] + '</strong></span> <span class="recipientemail">(' + recipients[1] + ')</span>&nbsp;&nbsp;';
            });
            
            makeMsgModalHeader( $mailbox, $sender, value, $subject, $datetime, $recipientNames );
            makeModalNavBar($prevMsgId, $nextMsgId, $msgIndex, $msgCount);
            prepareAttachments($hasAttachments, value, attachments);
            addDetailsCollapse(value);
            makeMsgModalBody(value);
            makeMsgModalFooter();

        });
    } else {
        alert('An unknown error occured and this message cannot be retrieved.<br />Please record the message details (or take a screenshot) and <br />create a ticket with the Help-Desk.</h4>');
    }

    // getToolBar($mailbox);

    $('.modalmessageid').text($messageId);
    $('.modalchangekey').text($changeKey)
    $('.modalfoldername').text($mailbox);
    $('[data-tooltip="tooltip"]').tooltip();
};

var makeMsgModalHeader = function ( $mailbox, $sender, value, $subject, $datetime, $recipientNames )
{
    $('.modal-title').html('<span class="modalmailbox">' + $mailbox + '<span class="pull-right count-tracker"></span></span>');
    $('.modal-emailheading').append('<h5><span class="sendername"><strong>' + $sender +'</strong></span> (<span class="senderemail">'+ value.Sender + '</span>)</h5>');
    $('.modal-emailheading').append('<h5 class="modalsubject">Subject: <span class="subject">' + $subject + '</span></h5>');
    $('.modal-emailheading').append('<h5>Sent: <span class="sentdate">' + $datetime + '</span>');
    $('.modal-emailheading').append('<h5> To: ' + $recipientNames + '</h5>');            

    $('.modal-body').append('<div class="collapseButtonsLeft"></div><div class="collapseButtonsRight"></div>');
    $('.modal-body').append('<div class="collapse" id="msgDetailsCollapse"></div>');    
}

var makeMsgModalBody = function(value)
{
    $('.modal-body').append(value.Body._);
}

var makeModalNavBar = function($prevMsgId, $nextMsgId, $msgIndex, $msgCount)
{



    var $cola  = '<div class="btn-group btn-group-justified pull-left" role="group">';
        $cola += '<div class="btn-group" role="group">';
        $cola += '<button type="button" class="btn btn-darkblue modal-prev" data-prev="' + $prevMsgId + '"><span class="fa fa-chevron-left"></span>Prev</button>';
        $cola += '</div>';
        $cola += '<div class="btn-group" role="group">';
        $cola += '<button type="button" class="btn btn-darkblue modal-next" data-next="' + $nextMsgId + '">Next<span class="fa fa-chevron-right fa-right"></span></button>';
        $cola += '</div>';
        $cola += '</div>';

    var $colb  = '<div class="btn-group btn-group-justified pull-left" role="group">';
        $colb += '<div class="btn-group" role="group">';
        $colb += '<button type="button" class="btn btn-darkgrey modal-policysearch"><span class="fa fa-retweet"></span>Policy Search</button>';
        $colb += '</div>';
        $colb += '<div class="btn-group" role="group">';
        $colb += '<button type="button" class="btn btn-darkgrey modal-toolbox"><span class="fa fa-wrench"></span>Tools</button>';
        $colb += '</div>';
        $colb += '<div class="btn-group" role="group">';
        $colb += '<button type="button" class="btn btn-darkgrey modal-showheaders" aria-controls="msgDetailsCollapse" aria-expanded="false" data-target="#msgDetailsCollapse" data-toggle="collapse"><span class="fa fa-chevron-down"></span>Headers</button>';
        $colb += '</div>';
        $colb += '<div class="btn-group" role="group">';
        $colb += '<button type="button" class="btn btn-darkgrey modal-attachments disabled" aria-controls="attDetailsCollapse" aria-expanded="false" data-target="#attDetailsCollapse" data-toggle="collapse"><span class="fa fa-paperclip"></span>Attachments</button>';
        $colb += '</div>';
        $colb += '<div class="btn-group" role="group">';
        $colb += '<button type="button" class="btn btn-darkgrey modal-printer"><span class="fa fa-print"></span>Print</button>';
        $colb += '</div>';
        $colb += '</div>';

    $('#modal-tools').append('<div class="row modalnavbarrow"></div>');
    $('.modalnavbarrow').append('<div class="col-xs-3 col-sm-3 column-no-right-padding modalnavbarcola"></div>');
    $('.modalnavbarrow').append('<div class="col-xs-9 col-sm-9 column-no-left-padding modalnavbarcolb"></div>');
    $('.modalnavbarcola').append($cola);
    $('.modalnavbarcolb').append($colb);

    $('.count-tracker').html($msgIndex + '/' + $msgCount);

    if ($prevMsgId == null) {
        $('.modal-prev').addClass('disabled');
    } else {
        $('.modal-prev').removeClass('disabled');
    }

    if ($nextMsgId == null) {
        $('.modal-next').addClass('disabled');
    } else {
        $('.modal-next').removeClass('disabled');
    }    
}

var makeMsgModalFooter = function () 
{
    $('.modal-footer').append('<div class="pull-left modalMsgNav modalMsgFootNav"></div>');
}

var prepareAttachments = function ($hasAttachments, value, attachments) 
{

    if ($hasAttachments && value.AttachmentInfo != false) {
        $('.modal-body').append('<div class="collapse" id="attDetailsCollapse"></div>');
        $('#attDetailsCollapse').append('<div class="well well-sm" id="attDetailsWell"></div>'); 
        $('#attDetailsWell').append('<table id="attachmentTable" ></table>');
        $.each(attachments, function(index, value) {  
            if (value.Inline != 1) {
                $icon = getAttachmentIcon(value.Name);
                $('#attachmentTable').append('<tr><td><button class="btn btn-sm btn-warning attachmentbutton" type="button" data-attid="' + value.Id + '"><span class="fa ' + $icon + '"></span></button></td><td class="attachmentFileName">' + value.Name + '</td><td>|<td><td class="text-right">' + sizeFormat(value.Size) + '</td><td></td></tr>');
            }
        });
    } else if ($hasAttachments && value.AttachmentInfo == false) {
        $('.modal-body').prepend('<div class="alert alert-danger" role="alert">The attachments for this email could not be retrieved. They may be retrieved through Outlook.</div>');
    }

    if ($hasAttachments){
        $('.modal-attachments').removeClass('disabled');
    } else {
        $('.modal-attachments').addClass('disabled');
    }    
}

var addDetailsCollapse = function(value)
{

    $msgDate = new Date(value.LastModifiedTime);
    $msgDate = $msgDate.format("ddd mm/dd/yy\xa0h:mm tt");  // \xa0 is the non-breaking space
    $('#msgDetailsCollapse').append('<div class="well well-sm" id="msgDetailsWell"></div>');
    $('#msgDetailsWell').html('<label>Last Modified By:</label><span class="lastmodifiedby">' + value.LastModifiedName + '</span><label>At:</label><span class="lastmodifiedat">' + $msgDate + '</span>');    
}
