 function sizeThisString($string, stringsize, $tail)
 {

    if ($string.length > stringsize) {
        $newstring = $string.substring(0,stringsize) + $tail;
    } else {
        $newstring = $string;
    } 
    return $newstring;   
 }

function getToolBar(mailbox) {

    if (mailbox == 'Inspections') {
        mailboxIcon = '<span class="fa fa-search context-icon"></span>';
    }    
    if (mailbox == 'Claims') {
        mailboxIcon = '<span class="fa fa-money context-icon"></span>';
    }
    if (mailbox == 'Quotes') {
        mailboxIcon = '<span class="fa fa-quote-right context-icon"></span>';
    }    
    if (mailbox == 'Info') {
        mailboxIcon = '<span class="fa fa-info context-icon"></span>';
    }    
    if (mailbox == 'Drop') {
        mailboxIcon = '<span class="fa fa-chevron-up context-icon"></span>';
    }    
    if (mailbox == 'Pull') {
        mailboxIcon = '<span class="fa fa-chevron-down context-icon"></span>';
    }    
    if (mailbox == 'Request') {
        mailboxIcon = '<span class="fa fa-cog context-icon"></span>';
    }   
    if (mailbox == 'Junk') {
        mailboxIcon = '<span class="fa fa-trash context-icon"></span>';
    }    
    if (mailbox == 'IT') {
        mailboxIcon = '<span class="fa fa-laptop context-icon"></span>';
    }    
    if (mailbox == 'Professional') {
        mailboxIcon = '<span class="fa fa-briefcase context-icon"></span>';
    } 



    $toolbar  = '      <button type="button" class="btn btn-default">' + mailboxIcon + mailbox + '</button>';
    $toolbar += '<div class="btn-group" role="group">';
    $toolbar += '      <button type="button" id="forward_dialog" class="btn btn-default dropdown-toggle forwardbutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-tooltip="tooltip" title="Forward"><span class="fa fa-share context-icon"></span>Forward</button>';
    $toolbar += '      <ul class="dropdown-menu">';
    $toolbar += '          <li><a class="dropdown-item forwardto" href="#"><label>To:</label><input class="forwardrecipient"></input></a></li>'; 
    $toolbar += '          <li><a class="dropdown-item forwardto" href="#"><label>Msg:</label><textarea class="forwardmessage"></textarea></a></li>';     
    $toolbar += '          <li><a class="dropdown-item forwardto" href="#"><button type="button" class="btn btn-default btn-xs pull-right sendforward disabled"><span class="fa fa-paper-plane"></span>&nbsp;&nbsp;Send</button></a></li>';      
    $toolbar += '      </ul>';        
    $toolbar += '      <button type="button" class="btn btn-default disabled" data-tooltip="tooltip" title="Notify someone"><span class="fa fa-exclamation-triangle context-icon"></span>Escalate</button>';
    $toolbar += '      <button type="button" class="btn btn-default disabled" data-tooltip="tooltip" title="Translate to English"><span class="fa fa-globe context-icon"></span>Translate</button>';
    $toolbar += '<div class="btn-group" role="group">';    
    $toolbar += '      <button type="button" id="dialog_link" class="btn btn-default dropdown-toggle movebutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-tooltip="tooltip" title="Move to another mailbox"><span class="fa fa-arrows context-icon"></span>Move</button>';
    $toolbar += '      <ul class="dropdown-menu">';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Inspections</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Claims</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Quotes</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Info</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Drop File</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Pull File</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Request</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">IT</a></li>';    
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Junk</a></li>';
    $toolbar += '          <li><a class="dropdown-item moveto" href="#">Professional</a></li>';    
    $toolbar += '      </ul>';    
    $toolbar += '</div>';    
    $toolbar += '<div class="btn-group" role="group">';    
    $toolbar += '      <button type="button" class="btn btn-default dropdown-toggle deletebutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-tooltip="tooltip" title="Delete permanently"><span class="fa fa-trash-o context-icon"></span>Delete</button>';
    $toolbar += '      <ul class="dropdown-menu">';
    $toolbar += '          <li><a class="dropdown-item delete-confirm" href="#">Delete</a></li>';
    $toolbar += '          <li><a class="dropdown-item delete-cancel" href="#">Cancel</a></li>';   
    $toolbar += '      </ul>';    
    $toolbar += '</div>';    
    $toolbar += '<div class="modalfoldername"></div><div class="modalmessageid"></div><div class="modalchangekey"></div>';
    
    $('#modal-tools').html($toolbar);

    
}


var getAttachmentIcon = function ($filename)
{
    $icon = 'fa-folder-open';
    $ext = $filename.substr($filename.lastIndexOf('.')+1).toUpperCase();
    if ($ext == 'DOC' || $ext == 'DOCX') {
        $icon = 'fa-file-word-o';
    }
    if ($ext == 'PDF') {
        $icon = 'fa-file-pdf-o';
    }   
    if ($ext == 'JPG' || $ext == 'PNG' || $ext == 'GIF' || $ext == 'TIF' || $ext == 'JPEG' || $ext == 'TIFF' || $ext == 'BMP') {
        $icon = 'fa-picture-o';
    }
    if ($ext == 'XLS' || $ext == 'XLSX' || $ext == 'CSV') {
        $icon = 'fa-file-excel-o';
    }
    if ($ext == 'MP4' || $ext == 'AVI' || $ext == 'MOV' || $ext == 'MKV' || $ext == 'WMV' || $ext == 'MJPEG' || $ext == '3GP' || $ext == 'MKS' || $ext == 'MPG' || $ext == 'MPG4' || $ext == 'XVID' || $ext == 'MV' || $ext == '3G2' || $ext == 'ASF' || $ext == 'ASX' || $ext == 'FLV' || $ext == 'RM' || $ext == 'SWF' || $ext == 'VOB'){
        $icon = 'fa-film';
    }  
    if ($ext == 'AIF' || $ext == 'IFF' || $ext == 'M3U' || $ext == 'M4A' || $ext == 'MID' || $ext == 'MP3' || $ext == 'MPA' || $ext == 'RA' || $ext == 'WAV' || $ext == 'WMA'){
        $icon = 'fa-voume-up';
    }    
    if ($ext == 'TXT' || $ext == 'LOG' || $ext == 'RTF' || $ext == 'TEX'){
        $icon = 'fa-file-text';
    }                  
    return $icon;
}

var sizeFormat = function (size) {

    $scale = 'B';
    $msgSize = size;

    if ($msgSize > 1024) {
        $msgSize = ($msgSize / 1024).toFixed(0);
        $scale = 'K';
    }
    if ($msgSize > 1024) {
        $msgSize = ($msgSize / 1024).toFixed(0);
        $scale = 'M';
    }                    
    if ($msgSize > 1024) {
        $msgSize = ($msgSize / 1024).toFixed(0);
        $scale = 'G';
    } 

    return $msgSize + '' + $scale;
}

function getAgeInDays($dateReceived)
{
    if ($dateReceived == '') {
        return '0';

    } else {
        if ($dateReceived.length > 10) {
            $fixedDate = $dateReceived.substring(0,10);
        } else {
            $fixedDate = $dateReceived;
        }
        $today = new Date();
        $tmpMsgDate = new Date($fixedDate);
        $age = Math.abs($today.getTime() - $tmpMsgDate.getTime());
        $age = Math.ceil($age / (1000*3600 * 24));

        return $age;
    }
}

var highlight = function(subject) {
    subject=subject.replace(/rush/gi, '<span class="highlight">RUSH</span>');
    subject=subject.replace(/urgent/gi, '<span class="highlight">URGENT</span>');
    subject=subject.replace(/au plus vite/gi, '<span class="highlight">URGENT</span>');
    subject=subject.replace(/a.s.a.p/gi, '<span class="highlight">URGENT</span>');
    subject=subject.replace(/important/gi, '<span class="highlight">URGENT</span>');
    subject=subject.replace(/urgence/gi, '<span class="highlight">URGENT</span>');

    subject=subject.replace(/\*\*\*\*spam\*\*\*\*/gi, '<span class="spam">****SPAM****</span>');

    return subject;
}

var addModalSidebar = function ()
{
    var $sidebar  = '<div id="msgSidebar" class="sidebar fade" role="dialog">';
        $sidebar += '<div class="sidebar-dialog sidebar-lg">';
        $sidebar += '<!-- Sidebar content-->';
        $sidebar += '<div class="sidebar-content">';
        $sidebar += '<div class="sidebar-header">';
        $sidebar += '<button type="button" class="close" data-dismiss="sidebar">&times;</button>';
        $sidebar += '<h4 class="sidebar-title text-left">Sidebar Header</h4>';
        $sidebar += '</div>';
        $sidebar += '<div id="sidebar-tools"></div>';
        $sidebar += '<div class="sidebar-body text-left">';
        $sidebar += '<p>Some text in the sidebar.</p>';
        $sidebar += '</div>';
        $sidebar += '<div class="sidebar-footer">';
        $sidebar += '<button type="button" class="btn btn-default" data-dismiss="sidebar">Close</button>';
        $sidebar += '</div>';
        $sidebar += '</div>';
        $sidebar += '</div>';
        $sidebar += '</div>';

    $('body').append($sidebar);
}

var msgModalReset = function() {

    var $nativeModal  = '<div class="modal-dialog modal-lg">';

        $nativeModal += '<div class="modal-content">';
        $nativeModal += '<div class="modal-header">';
        $nativeModal += '<h4 class="modal-title">Modal Header<span class="pull-right count-tracker"></span></h4>';
        $nativeModal += '</div>';
        $nativeModal += '<div class="modal-emailheading"></div>';
        $nativeModal += '<div id="modal-tools">';
           
        $nativeModal += '</div>';
        $nativeModal += '<div class="modal-body text-left">';
        $nativeModal += '<p>Some text in the modal.</p>';
        $nativeModal += '</div>';
        $nativeModal += '<div class="modal-footer">';
        $nativeModal += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        $nativeModal += '</div>';
        $nativeModal += '</div>';
        $nativeModal += '</div>';  

    $('#msgModal').html($nativeModal);
}
 
function fadeAlert(){
    if($("#alert").find("div").attr("data-fade") == "true"){
        $("#alert").delay(4000).fadeOut(3000);
        $("#alert").mouseover(function(){
            $(this).stop(true).animate({opacity:'100'});
        }).mouseleave(function(){
            $("#alert").delay(1000).fadeOut(2000);
        });
    }
    $.each($('.oldestmessage'), function(index, value){
        if (getAgeInDays($(value).text()) > 1) {
            $(value).text((getAgeInDays($(value).text())) + ' days');
        } else if (getAgeInDays($(value).text()) == 1) {
            $(value).text((getAgeInDays($(value).text())) + ' day');        
        } else {
            $(value).text('');
        }
    });
    $.each($('.messagecount'), function(index, value){
        if ($(value).text() > 1) {
            $(value).text($(value).text() + ' msgs');
        } else if ($(value).text() == 1) {
            $(value).text($(value).text() + ' msg');        
        } else {
            $(value).text('');
        }
    });
}

function printModal($title, $recipientName, $senderName, $senderEmail, $dateSent, $subject, $attachmentList, $body)
{
    if (printWindow) {
        printWindow.close();
    }

    printWindow = window.open("", "Test", "width=1000px, height=800px, menubar=no, scrollbars=yes, status=no, titlebar=no, toolbar=no, location=no, resizeable=yes");        

    $content  = "<html>";
    $content += "<head>";
    $content += "<style>";
    $content += "#print { font-family:sans-serif; font-size:14px}";
    $content += "#print label { font-weight: bold; min-width:130px; display:inline-block;}";
    $content += "#print h2 { margin-bottom:0px; padding-bottom:0px; font-size:16px;}";
    $content += "#print hr { margin-bottom:16px; padding-bottom:0px; height:3px;}";    
    $content += "#attDetailsCollapse { display:none; }";     
    $content += "#msgDetailsCollapse { display:none; }";
    $content += "</style>";
    $content += "<title>Print Message</title>";
    $content += "</head>";
    $content += "<body>";
    $content += "<div id='print'>";
    $content += "<h2>" + $recipientName + "</h2>";
    $content += "<hr>";
    $content += "<label>From:</label>" + $senderName + ' <' + $senderEmail + '><br/>';
    $content += "<label>Sent:</label>" + $dateSent + '<br/>';
    $content += "<label>Subject:</label>" + $subject + '<br/>';
    $content += "<label>Attachments:</label>" + $attachmentList + '<br/>';
    $content += "<br/><br/>";    
    $content += $body;
    $content += "</print>";
    $content += "</body>";

    printWindow.document.write($content);
    printWindow.focus();
    printWindow.window.print();
}

/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};

