    (function worker() {
      $.ajax({
        url: 'update', 

        success: function(data) {
            $.each(data, function(index, value) {
                $existingCount = $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.messagecount').html();
                $existingCount = $existingCount.substr(0,$existingCount.indexOf(' '));
                $existingAge = $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.oldestmessage').html();
                $existingAge = $existingAge.substr(0,$existingAge.indexOf(' '));
                if (value.OldestMessageDate != null) {
                    $oldestAge = getAgeInDays(value.OldestMessageDate);
                } else {
                    $oldestAge = 0;
                }
                if ($existingCount != value.Count) {
                    threshold = $('*[data-mailbox="' + value.Mailbox + '"]').data('scale');
                    if ($('*[data-mailbox="' + value.Mailbox + '"]').data('status') == 'open') {
                        updateDetails(value.Mailbox, threshold)
                        $('#message-details-panel').show();                    }
                }
                if (value.Count > 1 ) {
                    $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.messagecount').html(value.Count + ' msgs');
                } else if (value.Count == 1) {
                    $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.messagecount').html(value.Count + ' msg');
                } else {
                    $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.messagecount').html('');  
                }
                
                if ($oldestAge > 1) {
                    $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.oldestmessage').html($oldestAge + ' days');
                } else if ($oldestAge == 1) {
                    $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.oldestmessage').html($oldestAge + ' day');
                } else {
                    $('*[data-mailbox="'+ value.Mailbox +'"]').find('span.oldestmessage').html(''); 
                }
            })
            
            return data;
        },
        complete: function() {
          // Schedule the next request when the current one's complete
          setTimeout(worker, 5000);
        }
      });
    })();


 function updateDetails(mailbox, threshold) {

     var formData = new FormData(); 
     formData.append('mailbox', mailbox);                  
     $.ajax({                                                                    
        url: 'msgs',
        method: 'post',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: formData,
        success: function(data) {
            $msgCount = Object.keys(data).length;
            if (data[0] == 'Empty') {
                $('#message-details-panel').hide();
            } else {
            $('.mailbox-message').remove();
            $('#message-details-panel').show();
            $('.mailbox-name-panel').text(mailbox);
            $('.mailbox-count-panel').text('Messages: ' + $msgCount);
            $.each(data, function(index, value) {                  
                
                $sizeInK = sizeFormat(value.Size);                 
                $age = getAgeInDays(value.DateTimeReceived)
                $msgDate = new Date(value.DateTimeReceived);
                $msgDate = $msgDate.format("ddd mm/dd/yy\xa0h:mm tt");  // \xa0 is the non-breaking space
                if (value.Importance != 'Normal') {
                    $urgent = '<span class="highlight">!</span>';
                } else {
                    $urgent = '';
                }
                if (value.Attachments) {
                    $attachments = '<i class="fa fa-paperclip"></i>';
                } else {
                    $attachments = '';
                }
                $Subjectx = sizeThisString(value.Subject, 110, '...');
                $Subjectx = highlight($Subjectx);
                $number = index + 1;
                $('.list-group-message-details').append('<a id="message' + index + '" class="mailbox-message context-menu list-group-item" data-menutitle="Message&nbsp;' + (index + 1) +'" data-id='+value.Id+' data-attachments='+value.Attachments+' data-count='+$msgCount+' data-index='+$number+' data-mailbox=' + mailbox + ' href="#"></a>');
                if ($age >= (threshold/2)) {
                    $('#message'+index).addClass('warning');
                }                    
                if ($age == threshold) {
                    $('#message'+index).removeClass('warning');
                    $('#message'+index).addClass('info');
                }                    
                if ($age > threshold) {
                    $('#message'+index).addClass('danger');
                }
                $Sender = sizeThisString(value.Sender.Mailbox.Name, 30, '...');
                $Sender = $Sender.replace('faxserver@amfredericks.com', '<span class="fax">FAX SERVER</span>');
                $('#message'+index).append('<span class="messageid-fix">'+ $number +'.</span>');
                $('#message'+index).append('<span class="messagestatus-fix">'+ $urgent +'</span>');
                $('#message'+index).append('<span class="messagesender-fix sender">'+ $Sender +'</span>');
                $('#message'+index).append('<span class="messagesubject-fix subject">'+ $Subjectx +'</span>');
                $('#message'+index).append('<span class="messageage-fix">'+ $age + '\xa0d</span>');
                $('#message'+index).append('<span class="messagesent-fix msgdatetime hidden-md">'+ $msgDate + '</span>');
                $('#message'+index).append('<span class="messagesize-fix hidden-md">'+ $sizeInK +'</span>');
                $('#message'+index).append('<span class="messageattachment">'+ $attachments +'</span>');
            });
            $('#details').show();
            return data;
        } 
        }              
    });

}

var fetchMsg = function($source) {
    var $prevMsgId      = $source.prev().data('id');
    var $messageId      = $source.data('id');
    var $nextMsgId      = $source.next().data('id');
    var $msgCount       = $source.data('count');
    var $msgIndex       = $source.data('index');
    var $hasAttachments = $source.data('attachments');
    var $subject        = $source.find('.subject').html();
    var $sender         = $source.find('.sender').html();
    var $datetime       = $source.find('.msgdatetime').html();
    var $mailbox        = $source.data('mailbox');
    var formData = new FormData(); 
     formData.append('messageId', $messageId);                  
     $.ajax({                                                                    
        url: 'msg',
        method: 'post',
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        data: formData,
        success: function(data) {
            $('.modal-title').empty();
            $('.modal-body').empty();
            $('.modalMsgFootNav').remove();
            $('.collapseButtonsLeft').remove();                
            showMsg($prevMsgId, $messageId, $nextMsgId, $msgCount, $msgIndex, $hasAttachments, $subject, $sender, $datetime, $mailbox, '', data);
            $('.modal-body').show();
        },
        error: function() {
            showMsg($prevMsgId, $messageId, $nextMsgId, $msgCount, $msgIndex, $hasAttachments, $subject, $sender, $datetime, $mailbox, 'error');
        }
    })
} 

var showAttachments = function($attid) 
{
    console.log($attid);
    var formData = new FormData(); 
    formData.append('attachmentId', $attid);
    $.ajax({                                                                    
        url: 'attachment',
        method: 'post',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: formData,
        success: function(data) {
            if (data.meta) {
                $('#metaDetailsCollapse').remove();
                $('#attDetailsCollapse').after('<div id="metaDetailsCollapse" class="collapse in" aria-expanded="true"><button type="button" class="close metawellclose">&times;</button></div>');
                $('#metaDetailsCollapse').append('<div id="metaDetailsWell" class="well"></div>');
                $('#metaDetailsWell').append('<h4>Subject:' + data.subject + '</h4>');
                $('#metaDetailsWell').append('<h5>From:' + data.sender.Mailbox.Name + '</h5>');
                $('#metaDetailsWell').append('<hr/>');
                if (data.hasattachments) {
                    if (data.attachments.FileAttachment) {
                        if (data.attachments.FileAttachment.Name) {
                            if (!data.attachments.FileAttachment.IsInline) {
                                $icon = getAttachmentIcon(data.attachments.FileAttachment.Name);
                                $('#metaDetailsWell').append('<div id="multimeta"></div>');
                                $('#multimeta').append('<li class="metaattachment"><button class="btn btn-xs btn-info attachmentbutton" type="button" data-attid="' + value.AttachmentId.Id + '"><span class="fa ' + $icon + '"></button> ' + value.Name + '  |  ' + sizeFormat(value.Size) + '</span></li>');
                            }
                        } else {
                            $('#metaDetailsWell').append('<div id="multimeta"></div>');
                            $.each(data.attachments.FileAttachment, function(index, value) {
                                if (!value.IsInline) {
                                    $icon = getAttachmentIcon(value.Name);
                                    $('#multimeta').append('<li class="metaattachment"><button class="btn btn-xs btn-info attachmentbutton" type="button" data-attid="' + value.AttachmentId.Id + '"><span class="fa ' + $icon + '"></button> ' + value.Name + '  |  ' + sizeFormat(value.Size) + '</span></li>');
                                }   
                            });
                        }
                    }
                    if (data.attachments.ItemAttachment) {
                        if (data.attachments.ItemAttachment.Name) {
                            if (!data.attachments.ItemAttachment.IsInline) {
                                $icon = getAttachmentIcon(data.attachments.ItemAttachment.Name);
                                $('#metaDetailsWell').append('<li class="metaattachment"><button class="btn btn-xs btn-warning attachmentbutton" type="button" data-attid="' + data.attachments.ItemAttachment.AttachmentId.Id + '"><span class="fa ' + $icon + '"></button> ' + data.attachments.ItemAttachment.Name + '</span></li>');
                            }                                
                        } else {
                            $('#metaDetailsWell').append('<div id="multimeta"></div>');
                            $.each(data.attachments.ItemAttachment, function(index, value) {
                                if (!value.IsInline) {
                                    $icon = getAttachmentIcon(value.Name);
                                    $('#multimeta').append('<li class="metaattachment"><button class="btn btn-xs btn-warning attachmentbutton" type="button" data-attid="' + value.AttachmentId.Id + '"><span class="fa ' + $icon + '"></button> ' + value.Name + '</span></li>');
                                }
                            });                        
                        }
                    }
                }
                ;
                $('#metaDetailsWell').append('<p class="clearfix"><hr />' + data.body._ + '</p>');

            } else {
                window.open(data);
            }
        }
    });
};          