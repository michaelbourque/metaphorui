var printWindow;

$(document).ready(function() {

    fadeAlert();
    msgModalReset(); 

    $('#message-details-panel').hide();

    $(".toggle-view").click(function(e){
            e.preventDefault();
            if($(this).children("span").attr("class") == "glyphicon glyphicon-plus"){
                $(this).closest(".panel").find(".panel-body").fadeToggle(100, "linear");
                $(this).closest(".panel").find("table").fadeToggle(100, "linear");
                $(this).children("span").attr("class", "glyphicon glyphicon-minus");
            } else {
                $(this).closest(".panel").find(".panel-body").fadeToggle(300, "linear");
                $(this).closest(".panel").find("table").fadeToggle(300, "linear");
                $(this).children("span").attr("class", "glyphicon glyphicon-plus");
            }
        }
    );

    $(".cs_dropdown").on("click", function(e){
        var open = $(this).attr("data-open");
        if(open == "false"){
            $(this).attr("data-open", "true");
            $(this).find(".cs_dropdown_caret").attr("class", "fa fa-caret-down cs_dropdown_caret");
            $(this).siblings(".cs_dropdown_body").removeAttr("style");
        } else {
            $(this).attr("data-open", "false");
            $(this).find(".cs_dropdown_caret").attr("class", "fa fa-caret-right cs_dropdown_caret");
            $(this).siblings(".cs_dropdown_body").hide();
        }
    });

    $('.mailbox-list-item').on("click", function() {
     
        var mailbox = $(this).attr('data-mailbox');
        var threshold = $(this).attr('data-scale');
        $('.mailbox-list-item').attr('data-status', 'closed');
        $('.mailbox-list-item').removeClass('active');
        $(this).attr('data-status', 'open');
        $(this).addClass('active');
        updateDetails(mailbox, threshold);

    });  

    $('#msgModal').on('hidden.bs.modal', function () {
        $('.mailbox-message').removeClass('active');
    })    
});

$('body').on('click', '.mailbox-message', function(e) {
    $('.mailbox-message').removeClass('active');
    $(this).addClass('active');
    msgModalReset();
    $source = $(this);
    fetchMsg($source);
    $('#msgModal').modal('toggle');    
});

$('body').on('click', 'button.disabled',function(e){
    e.stopImmediatePropagation();
    e.preventDefault();
});

$('body').on('click', '.modal-next', function(e) {
    $('.mailbox-message').removeClass('active');
    $("a[data-id='" + $(this).data('next') + "']").addClass('active');    
    $source = $("a[data-id='" + $(this).data('next') + "']");
    $('#msgModal').hide();
    msgModalReset(); 
    fetchMsg($source);
    $('#msgModal').show();
});

$('body').on('click', '.modal-prev', function(e) {
    $('.mailbox-message').removeClass('active');
    $("a[data-id='" + $(this).data('prev') + "']").addClass('active');    
    $source = $("a[data-id='" + $(this).data('prev') + "']");
    $('#msgModal').hide();
    msgModalReset(); 
    fetchMsg($source);
    $('#msgModal').show();
});


$('body').on('click', '.modal-printer', function(e) {  
    
    var $recipientName = $('.recipientname').map(function(i, opt) {
        return $(opt).text();
    }).toArray().join(', ');  
    var $attachmentList = $('.attachmentFileName').map(function(i, opt) {
        return $(opt).text();
    }).toArray().join('<br/><label></label>');       
    var $senderName = $('.sendername').text();
    var $senderEmail = $('.senderemail').text();
    var $dateSent = $('.sentdate').text();
    var $subject = $('.modalsubject').find('.subject').text();
    //var $attachmentList = 
    var $modifiedBy = $('.lastmodifiedby').text();
    var $modifiedAt = $('.lastmodifiedat').text();
    var $body = $('.modal-body').html();
    $($body).hide('#msgDetailsWell');
    printModal('Test', $recipientName, $senderName, $senderEmail, $dateSent, $subject, $attachmentList, $body);
});


$('body').on('click', '.attachmentbutton', function(e) {
    var $attid = $(this).data('attid');
    showAttachments($attid);
});

$('body').on('click', '.metawellclose', function() {
    $('#metaDetailsCollapse').remove();
});

